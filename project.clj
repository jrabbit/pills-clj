(defproject pills-simple "0.2.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "https://0xacab.org/jrabbit/pills-clj/"
  :license {:name "AGPL-3.0" :url "https://www.gnu.org/licenses/agpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.10.3"]
  [clojure.java-time "0.3.2"]
  [tick "0.4.31-alpha"]
  [org.clojure/tools.cli "1.0.194"]
  [ahungry/xdg-rc "0.0.4"]
  [clojure-humanize "0.2.2"]
  ]
  :plugins [[lein-cljfmt "0.7.0"]]
  :main ^:skip-aot pills-simple.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
