(ns pills-simple.core
  (:require
   [tick.alpha.api :as t]
   [clojure.edn]
   [clojure.tools.cli :refer [parse-opts]]
   [time-literals.data-readers]
   [time-literals.read-write]
   [xdg-rc.core :as xdg-rc]
   [clojure.string :as str]
   [clojure.java.io :as io]
   [clojure.contrib.humanize])
  (:gen-class))

(time-literals.read-write/print-time-literals-clj!)

(def cli-options
  [["-d" "--dry-run"], ["-n" "--db-name"]])

(defn occurance
  ([time drug] {:time time, :name drug})
  ([time drug dose] {:time time, :name drug, :dose dose}))

(defn -main
  "entrypoint for script"
  [& args]
  (def conf (parse-opts args cli-options))
  (def database (if (:options :db-name) (:options :db-name)  "gamer"))
  (def os (first (str/split (System/getProperty "os.name") #" ")))
  (if (= os "Windows")
    ; make path in windows
    (io/make-parents (str (System/getenv "LOCALAPPDATA") "\\pills-clj\\" database ".db"))
    (xdg-rc.core/make-configs! "pills-clj" (str database ".db")))
  (let [path (cond
               (= os "Windows") (str (System/getenv "LOCALAPPDATA") "\\pills-clj\\" database ".db")
               (str/starts-with? os "Mac") (str (System/getenv "HOME") "/Library/Application Support/pills-clj/" database ".db")
               :else (xdg-rc.core/with-xdg "pills-clj" (str database ".db")))]
    (try
      (def x (clojure.edn/read-string {:readers time-literals.read-write/tags} (slurp path)))
      (catch java.io.FileNotFoundException e (def x [])))
    (cond (some #(= "report" %) (:arguments conf))
          (do (println "report:") (println (map (fn [a] (clojure.contrib.humanize/datetime (- t/zoned-date-time (:time a))))) x))
          (some #(= "export" %) (:arguments conf))
          (do (println "do something")))
    (def z (pr-str (cons (occurance (t/zoned-date-time) "test") x)))
    (if (:options :dry-run) ()
        (spit path z))
    (println z)))
